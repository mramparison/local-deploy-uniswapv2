const { ethers } = require("hardhat");
import { UniswapV2Pair, ERC20 } from "../typechain-types";
import { BigNumber, Contract } from 'ethers'
//import { bigNumberify } from 'ethers'
import { expandTo18Decimals, encodePrice, shortenTo18Decimals } from "../src/test/shared/utilities";
const Web3 = require('web3');
const web3 = new Web3('http://localhost:8545');
const MINIMUM_LIQUIDITY = BigNumber.from(10).pow(3);

async function main() {

    const [wallet, other1, other2] = await ethers.getSigners();
    const token = await ethers.getContractFactory("contracts/test/ERC20.sol:ERC20");
    //console.log([wallet, other1, other2]);

    // deploy tokens
    console.log('===> Deploy two tokens...\n');
    const tokenA = await token.deploy(expandTo18Decimals(10000));
    const addressA = await tokenA.address;
    console.log(`Token A Address: ${addressA}`); 
    const tokenB = await token.deploy(expandTo18Decimals(10000));
    const addressB = await tokenB.address;
    console.log(`Token B Address: ${addressB}`);

    const weth = await ethers.getContractFactory("WETH9");
    const WETH = await weth.deploy();

    const erc20 = await ethers.getContractFactory("contracts/test/ERC20.sol:ERC20");
    const WETHPartner = await erc20.deploy(expandTo18Decimals(10000));

    // deploy V2
    console.log('===> Getting the Factory contract...\n');
    const v2factory = await ethers.getContractFactory("UniswapV2Factory");
    const factoryV2 = await v2factory.deploy(wallet.address);

    const routerEmit = await ethers.getContractFactory("RouterEventEmitter");

    const RouterEmit = await routerEmit.deploy();

    // deploy routers
    console.log('===> Getting the Router contract...\n');
    const router = await ethers.getContractFactory("UniswapV2Router");
    const router02 = await router.deploy(factoryV2.address, WETH.address);

    // initialize V2
    console.log('===> Create Pair...\n');
    await factoryV2.createPair(tokenA.address, tokenB.address);
    const pairAddress = await factoryV2.getPair(tokenA.address, tokenB.address);
    const pairFactory = await ethers.getContractFactory("UniswapV2Pair");
    const pair = new Contract(
      pairAddress,
      pairFactory.interface,
      wallet
    ) as UniswapV2Pair;

    console.log('===> Determine who\'s who...\n');
    const token0Address = await pair.token0();
    const token0 = tokenA.address === token0Address ? tokenA : tokenB;
    tokenA.address === token0Address ? console.log("TokenA is") : console.log("TokenB is") ;
    console.log(`Token0 Address: ${token0.address}`);
    const token1 = tokenA.address === token0Address ? tokenB : tokenA;
    tokenA.address === token0Address ? console.log("TokenB is") : console.log("TokenA is") ;
    console.log(`Token1 Address: ${token1.address}\n`);

    await factoryV2.createPair(WETH.address, WETHPartner.address);
    const WETHPairAddress = await factoryV2.getPair(
      WETH.address,
      WETHPartner.address
    );

    const wethPair = new Contract(
      WETHPairAddress,
      pairFactory.interface,
      wallet
    );


    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)}`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()}`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)}`);
    console.log(`Other1 Pair Balance: ${await pair.balanceOf(other1.address)}`);
    console.log(`Other2 Pair Balance: ${await pair.balanceOf(other2.address)}`);
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )`);
    console.log(`Other1 TokenA Balance: ${await token1.balanceOf(other1.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other1.address))} )`);
    console.log(`Other1 TokenB Balance: ${await token0.balanceOf(other1.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other1.address))} )`);
    console.log(`Other2 TokenA Balance: ${await token1.balanceOf(other2.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other2.address))} )`);
    console.log(`Other2 TokenB Balance: ${await token0.balanceOf(other2.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other2.address))} )\n`);


    console.log('===> Transfer to Other1...\n');
    const token0Amount = expandTo18Decimals(100);
    const token1Amount = expandTo18Decimals(400);
    await token0.transfer(other1.address, token0Amount);
    await token1.transfer(other1.address, token1Amount);
    await token0.transfer(other1.address, token0Amount);
    await token1.transfer(other1.address, token1Amount);


    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)}`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()}`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)}`);
    console.log(`Other1 Pair Balance: ${await pair.balanceOf(other1.address)}`);
    console.log(`Other2 Pair Balance: ${await pair.balanceOf(other2.address)}`);
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )`);
    console.log(`Other1 TokenA Balance: ${await token1.balanceOf(other1.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other1.address))} )`);
    console.log(`Other1 TokenB Balance: ${await token0.balanceOf(other1.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other1.address))} )`);
    console.log(`Other2 TokenA Balance: ${await token1.balanceOf(other2.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other2.address))} )`);
    console.log(`Other2 TokenB Balance: ${await token0.balanceOf(other2.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other2.address))} )\n`);

    console.log('===> Transfer to Other2...\n');
    //const token0Amount = expandTo18Decimals(100);
    //const token1Amount = expandTo18Decimals(400);
    await token0.transfer(other2.address, token0Amount);
    await token1.transfer(other2.address, token1Amount);

    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)}`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()}`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)}`);
    console.log(`Other1 Pair Balance: ${await pair.balanceOf(other1.address)}`);
    console.log(`Other2 Pair Balance: ${await pair.balanceOf(other2.address)}`);
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )`);
    console.log(`Other1 TokenA Balance: ${await token1.balanceOf(other1.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other1.address))} )`);
    console.log(`Other1 TokenB Balance: ${await token0.balanceOf(other1.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other1.address))} )`);
    console.log(`Other2 TokenA Balance: ${await token1.balanceOf(other2.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other2.address))} )`);
    console.log(`Other2 TokenB Balance: ${await token0.balanceOf(other2.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other2.address))} )\n`);

   
    
    console.log('===> Add Liquidity Owner...\n');
    //const token0Amount = expandTo18Decimals(1);
    //const token1Amount = expandTo18Decimals(4);

    //const expectedLiquidity = expandTo18Decimals(2);
    await token0.approve(router02.address, ethers.constants.MaxUint256);
    await token1.approve(router02.address, ethers.constants.MaxUint256);
    router02.addLiquidity(
        token0.address,
        token1.address,
        token0Amount,
        token1Amount,
        0,
        0,
        wallet.address,
        ethers.constants.MaxUint256
    )

    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)}`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()}`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)}`);
    console.log(`Other1 Pair Balance: ${await pair.balanceOf(other1.address)}`);
    console.log(`Other2 Pair Balance: ${await pair.balanceOf(other2.address)}`);
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )`);
    console.log(`Other1 TokenA Balance: ${await token1.balanceOf(other1.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other1.address))} )`);
    console.log(`Other1 TokenB Balance: ${await token0.balanceOf(other1.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other1.address))} )`);
    console.log(`Other2 TokenA Balance: ${await token1.balanceOf(other2.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other2.address))} )`);
    console.log(`Other2 TokenB Balance: ${await token0.balanceOf(other2.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other2.address))} )\n`);


 /*    
   // Big check Allowances
   console.log(`==> Big check Allowances\n`);

    await token0.approve(wallet.address, ethers.constants.MaxUint256);
    
    await token0.approve(other1.address, ethers.constants.MaxUint256);
    await token1.approve(other1.address, ethers.constants.MaxUint256);
    await token0.approve(router02.address, ethers.constants.MaxUint256);
    await pair.approve(router02.address, ethers.constants.MaxUint256);
    await pair.approve(other1.address, ethers.constants.MaxUint256);
    console.log(`allowance owner over owner's wallet: ${(await token0.allowance(wallet.address, wallet.address))}`);
    console.log(`allowance other1 over owner's wallet: ${(await token0.allowance(wallet.address, other1.address))}`);
    console.log(`allowance other2 over owner's wallet: ${(await token0.allowance(wallet.address, other2.address))}`);
    console.log(`allowance router over owner's wallet: ${(await token0.allowance(wallet.address, router02.address))}`);
    console.log(`allowance router over other1's wallet: ${(await token0.allowance(wallet.address, router02.address))}`);
    console.log(`allowance other1 over token0's wallet:: ${(await token0.allowance(token0.address, other1.address))}`);
    console.log(`allowance owner over pair's wallet: ${(await pair.allowance(pair.address, wallet.address))}`);
    await pair.approve(wallet.address, ethers.constants.MaxUint256);
    console.log(`allowance owner over pair's wallet: ${(await token0.allowance(pair.address, wallet.address))}`);
    console.log(`allowance other1 over pair's wallet: ${(await token0.allowance(pair.address, other1.address))}`);

    await pair.approve(other1.address, ethers.constants.MaxUint256);
    console.log(`allowance router02 over pair's wallet: ${(await pair.allowance(pair.address, router02.address))}\n`);
 */

    //await token0.approve(other2.address, ethers.constants.MaxUint256);
    //console.log(`allowance other1 over owner's token0 wallet: ${(await token0.allowance(wallet.address, other1.address))}`);
    //console.log(`allowance other2 over owner's token0 wallet: ${(await token0.allowance(wallet.address, other2.address))}`);



    console.log('===> Transfer Other1 to Other2...\n');
   // check Allowances
    console.log(`allowance owner over other1's token0 wallet: ${(await token0.allowance(other1.address, wallet.address))}`);
    console.log(`allowance owner over other2's token0 wallet: ${(await token0.allowance(other2.address, wallet.address))}`);
    console.log(`=> approve owner over other 1 and 2's token0 wallets`);
    await token0.connect(other1).approve(wallet.address, ethers.constants.MaxUint256);
    await token0.connect(other2).approve(wallet.address, ethers.constants.MaxUint256);
    console.log(`allowance owner over other1's token0 wallet: ${(await token0.allowance(other1.address, wallet.address))}`);
    console.log(`allowance owner over other2's token0 wallet: ${(await token0.allowance(other2.address, wallet.address))}\n`);

    console.log(`allowance owner over other1's token1 wallet: ${(await token1.allowance(other1.address, wallet.address))}`);
    console.log(`allowance owner over other2's token1 wallet: ${(await token1.allowance(other2.address, wallet.address))}`);
    console.log(`=> approve owner over other 1 and 2's token1 wallets`);
    await token1.connect(other1).approve(wallet.address, ethers.constants.MaxUint256);
    await token1.connect(other2).approve(wallet.address, ethers.constants.MaxUint256);
    console.log(`allowance owner over other1's token1 wallet: ${(await token1.allowance(other1.address, wallet.address))}`);
    console.log(`allowance owner over other2's token1 wallet: ${(await token1.allowance(other2.address, wallet.address))}\n`);

    console.log(`=> transfer token0 from other1 to other2\n`);
    await token0.transferFrom(other1.address, other2.address, token0Amount);

  
    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)}`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()}`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)}`);
    console.log(`Other1 Pair Balance: ${await pair.balanceOf(other1.address)}`);
    console.log(`Other2 Pair Balance: ${await pair.balanceOf(other2.address)}`);
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )`);
    console.log(`Other1 TokenA Balance: ${await token1.balanceOf(other1.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other1.address))} )`);
    console.log(`Other1 TokenB Balance: ${await token0.balanceOf(other1.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other1.address))} )`);
    console.log(`Other2 TokenA Balance: ${await token1.balanceOf(other2.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other2.address))} )`);
    console.log(`Other2 TokenB Balance: ${await token0.balanceOf(other2.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other2.address))} )\n`);



    console.log('===> Add Liquidity Other2...\n');

/* 
    // addLiquidity sans Router
    console.log(`=> transfer token0 from other2 to pair`);
    await token0.transferFrom(other2.address, pair.address, token0Amount);
    console.log(`=> transfer token1 from other2 to pair`);
    await token1.transferFrom(other2.address, pair.address, token1Amount);
    console.log(`=> mint\n`);
    await pair.mint(other2.address);
 */
    
    
    // addLiquidity avec Router
    await token0.connect(other2).approve(router02.address, ethers.constants.MaxUint256);
    await token1.connect(other2).approve(router02.address, ethers.constants.MaxUint256);
    await router02.connect(other2).addLiquidity(
        token0.address,
        token1.address,
        token0Amount,
        token1Amount,
        0,
        0,
        other2.address,
        ethers.constants.MaxUint256
    )

//    await pair.approve(router02.address, ethers.constants.MaxUint256);

    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)}`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()}`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)}`);
    console.log(`Other1 Pair Balance: ${await pair.balanceOf(other1.address)}`);
    console.log(`Other2 Pair Balance: ${await pair.balanceOf(other2.address)}`);
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )`);
    console.log(`Other1 TokenA Balance: ${await token1.balanceOf(other1.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other1.address))} )`);
    console.log(`Other1 TokenB Balance: ${await token0.balanceOf(other1.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other1.address))} )`);
    console.log(`Other2 TokenA Balance: ${await token1.balanceOf(other2.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other2.address))} )`);
    console.log(`Other2 TokenB Balance: ${await token0.balanceOf(other2.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other2.address))} )\n`);




    console.log('===> Swap Other1...\n');
    const swapAmount = expandTo18Decimals(10);
    console.log(`allowance router over other1's token0 wallet: ${(await token0.connect(other1).allowance(other1.address, router02.address))}`);
    console.log(`allowance router over other1's token1 wallet: ${(await token1.connect(other1).allowance(other1.address, router02.address))}`);
    console.log(`=> approve router over other1 token0 and token1 wallets`);
    await token0.connect(other1).approve(router02.address, ethers.constants.MaxUint256);
    await token1.connect(other1).approve(router02.address, ethers.constants.MaxUint256);
    console.log(`allowance router over other1's token0 wallet: ${(await token0.connect(other1).allowance(other1.address, router02.address))}`);
    console.log(`allowance router over other1's token1 wallet: ${(await token1.connect(other1).allowance(other1.address, router02.address))}\n`);
    //await pair.approve(router02.address, ethers.constants.MaxUint256);
    //await token0.approve(other1.address, ethers.constants.MaxUint256);
    //await pair.approve(other1.address, ethers.constants.MaxUint256);

    await router02.connect(other1).swapExactTokensForTokens(
        swapAmount,
        0,
        [token0.address, token1.address], //swap token B for token A
        other1.address,
        ethers.constants.MaxUint256
    )

    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)}`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()}`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)}`);
    console.log(`Other1 Pair Balance: ${await pair.balanceOf(other1.address)}`);
    console.log(`Other2 Pair Balance: ${await pair.balanceOf(other2.address)}`);
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )`);
    console.log(`Other1 TokenA Balance: ${await token1.balanceOf(other1.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other1.address))} )`);
    console.log(`Other1 TokenB Balance: ${await token0.balanceOf(other1.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other1.address))} )`);
    console.log(`Other2 TokenA Balance: ${await token1.balanceOf(other2.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other2.address))} )`);
    console.log(`Other2 TokenB Balance: ${await token0.balanceOf(other2.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other2.address))} )\n`);



    console.log('===> Remove Liquidity Other2...\n');
    const expectedLiquidity = expandTo18Decimals(200);
    console.log(`allowance router over other2's pair wallet: ${(await pair.connect(other2).allowance(other2.address, router02.address))}`);
    console.log(`=> approve router over other1 pair wallet`);
    await pair.connect(other2).approve(router02.address, ethers.constants.MaxUint256);
    console.log(`allowance router over other2's pair wallet: ${(await pair.connect(other2).allowance(other2.address, router02.address))}\n`);

    await router02.connect(other2).removeLiquidity(
        token0.address,
        token1.address,
        expectedLiquidity.sub(MINIMUM_LIQUIDITY),
        0,
        0,
        other2.address,
        ethers.constants.MaxUint256
    ) 

    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)}`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()}`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)}`);
    console.log(`Other1 Pair Balance: ${await pair.balanceOf(other1.address)}`);
    console.log(`Other2 Pair Balance: ${await pair.balanceOf(other2.address)}`);
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )`);
    console.log(`Other1 TokenA Balance: ${await token1.balanceOf(other1.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other1.address))} )`);
    console.log(`Other1 TokenB Balance: ${await token0.balanceOf(other1.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other1.address))} )`);
    console.log(`Other2 TokenA Balance: ${await token1.balanceOf(other2.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other2.address))} )`);
    console.log(`Other2 TokenB Balance: ${await token0.balanceOf(other2.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other2.address))} )\n`);



}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exitCode = 1;
    });


