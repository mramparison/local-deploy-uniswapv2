const { ethers } = require("hardhat");
import { UniswapV2Pair, ERC20 } from "../typechain-types";
import { BigNumber, Contract } from 'ethers'
//import { bigNumberify } from 'ethers'
import { expandTo18Decimals, encodePrice, shortenTo18Decimals } from "../src/test/shared/utilities";
const Web3 = require('web3');
const web3 = new Web3('http://localhost:8545');
const MINIMUM_LIQUIDITY = BigNumber.from(10).pow(3);

async function main() {

    const [wallet] = await ethers.getSigners();
    const token = await ethers.getContractFactory("contracts/test/ERC20.sol:ERC20");

    // deploy tokens
    console.log('===> Deploy two tokens...\n');
    const tokenA = await token.deploy(expandTo18Decimals(10000));
    const addressA = await tokenA.address;
    console.log(`Token A Address: ${addressA}`); 
    const tokenB = await token.deploy(expandTo18Decimals(10000));
    const addressB = await tokenB.address;
    console.log(`Token B Address: ${addressB}`);

    const weth = await ethers.getContractFactory("WETH9");
    const WETH = await weth.deploy();

    const erc20 = await ethers.getContractFactory("contracts/test/ERC20.sol:ERC20");
    const WETHPartner = await erc20.deploy(expandTo18Decimals(10000));

    // deploy V2
    console.log('===> Getting the Factory contract...\n');
    const v2factory = await ethers.getContractFactory("UniswapV2Factory");
    const factoryV2 = await v2factory.deploy(wallet.address);

    const routerEmit = await ethers.getContractFactory("RouterEventEmitter");

    const RouterEmit = await routerEmit.deploy();

    // deploy routers
    console.log('===> Getting the Router contract...\n');
    const router = await ethers.getContractFactory("UniswapV2Router");
    const router02 = await router.deploy(factoryV2.address, WETH.address);

    // initialize V2
    console.log('===> Create Pair...\n');
    await factoryV2.createPair(tokenA.address, tokenB.address);
    const pairAddress = await factoryV2.getPair(tokenA.address, tokenB.address);
    const pairFactory = await ethers.getContractFactory("UniswapV2Pair");
    const pair = new Contract(
      pairAddress,
      pairFactory.interface,
      wallet
    ) as UniswapV2Pair;

    console.log('===> Determine who\'s who...\n');
    const token0Address = await pair.token0();
    const token0 = tokenA.address === token0Address ? tokenA : tokenB;
    tokenA.address === token0Address ? console.log("TokenA is") : console.log("TokenB is") ;
    console.log(`Token0 Address: ${token0.address}`);
    const token1 = tokenA.address === token0Address ? tokenB : tokenA;
    tokenA.address === token0Address ? console.log("TokenB is") : console.log("TokenA is") ;
    console.log(`Token1 Address: ${token1.address}\n`);

    await factoryV2.createPair(WETH.address, WETHPartner.address);
    const WETHPairAddress = await factoryV2.getPair(
      WETH.address,
      WETHPartner.address
    );

    const wethPair = new Contract(
      WETHPairAddress,
      pairFactory.interface,
      wallet
    );


    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)}`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()}`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)}`);
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )\n`);

 /*   
    console.log('===> Add Liquidity...\n');
    const token0Amount = expandTo18Decimals(1);
    const token1Amount = expandTo18Decimals(4);

    const expectedLiquidity = expandTo18Decimals(2);
    await token0.approve(router02.address, ethers.constants.MaxUint256);
    await token1.approve(router02.address, ethers.constants.MaxUint256);
    router02.addLiquidity(
        token0.address,
        token1.address,
        token0Amount,
        token1Amount,
        0,
        0,
        wallet.address,
        ethers.constants.MaxUint256
    )

    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)}`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()}`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)}`);
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )\n`);


    console.log('===> Add again...\n');
    //const token0Amount = expandTo18Decimals(1);
    //const token1Amount = expandTo18Decimals(4);
    await token0.transfer(pair.address, token0Amount);
    await token1.transfer(pair.address, token1Amount);
    await pair.mint(wallet.address);

    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)}`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()}`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)}`);
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )\n`);

    console.log('===> Remove Liquidity...\n');
    //const expectedLiquidity = expandTo18Decimals(2);
    await pair.approve(router02.address, ethers.constants.MaxUint256);
    router02.removeLiquidity(
        token0.address,
        token1.address,
        expectedLiquidity.sub(MINIMUM_LIQUIDITY),
        0,
        0,
        wallet.address,
        ethers.constants.MaxUint256
    ) 

    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)}`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()}`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)}`);
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )\n`);
*/
    console.log('===> Add again...\n');
    const token0Amount = expandTo18Decimals(5);
    const token1Amount = expandTo18Decimals(10);
    const swapAmount = expandTo18Decimals(1);
    const expectedOutputAmount = BigNumber.from("1662497915624478906");
    await token0.transfer(pair.address, token0Amount);
    await token1.transfer(pair.address, token1Amount);
    await pair.mint(wallet.address);

    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)}`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()}`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)}`);
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )\n`);
 
    console.log('===> Swap...\n');
    //const swapAmount = expandTo18Decimals(1);
    await token0.approve(router02.address, ethers.constants.MaxUint256);
    router02.swapExactTokensForTokens(
        swapAmount,
        0,
        [token0.address, token1.address],
        wallet.address,
        ethers.constants.MaxUint256
    )

    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)}`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()}`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)}`);
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )\n`);


}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exitCode = 1;
    });


