#!/bin/bash
if [ -z $2 ]; then PROF_MD=$(basename "$1" .md)_prof.md; else PROF_MD=$2;fi

awk 'BEGIN{prof = "ko"}
    /<!--PROF/  {prof = "getting_ok"}
    {
        if (prof == "ok"){
            if ( $0 ~ /-->/){
              prof = "ko"
              print ">";
              print "> *PROF* ";
              print "";
            }
            else  {
              print ">", $0;
            }
        }
        else {
          if (prof == "getting_ok"){
            prof="ok";
            print "> *PROF*";
            print ">";
          } else {
            print $0;
          }
        }
    }' "$1" > "$PROF_MD"
