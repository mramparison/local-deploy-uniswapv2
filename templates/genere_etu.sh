#!/bin/bash
if [ -z $2 ]; then ETU_MD=$(basename "$1" .md)_etu.md; else ETU_MD=$2;fi
awk 'BEGIN{prof = "ko"}
    /<!--PROF/  {prof = "ok"}
    {
      if (prof == "ok"){
        if ( $0 ~ /-->/){
          prof = "ko"
        }
      }
      else {
        print $0;
      }
    }' "$1" > "$ETU_MD"
