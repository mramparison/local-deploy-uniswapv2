TEMPLATES=templates
TEMPLATEDIRHTML=pandoc-bootstrap-template
TEMPLATEHTML=$(TEMPLATES)/$(TEMPLATEDIRHTML)/template.html
TEMPLATEPDF=$(TEMPLATES)/eisvogel.tex
TEMPLATECSS=$(TEMPLATES)/$(TEMPLATEDIRHTML)/template.css
TP=sujet-TP
NOTES=etapes-install
#SRC= $(TP) $(NOTES)
#FILES = $(wildcard $(TP)/*.md $(NOTES)/*.md )
FILES = $(wildcard $(TP)/*.md )
FILENAMES = $(FILES:.md=.toconvert)
TRGETSHTML = $(FILES:.md=.html)
TRGETSPDF = $(FILES:.md=.pdf)
.PHONY: all clean md html pdf pull help superclean
.DEFAULT_GOAL := help

all: pull md html pdf clean ## targets: md html pdf pull clean
#	echo $(FILES)
#	echo $(TRGETS)

md: $(FILENAMES)  ## generate *.md files prof & etu

%.toconvert: %.md
	$(TEMPLATES)/genere_prof.sh $< $(addsuffix _prof.md,$(basename $@))
	$(TEMPLATES)/genere_etu.sh $< $(addsuffix _etu.md,$(basename $@))

html: $(TRGETSHTML)  ## convert etapes-install/*_{prof, etu}.md to *.html

%.html: %.md $(TEMPLATES) $(TEMPLATEHTML) $(TEMPLATECSS) 
	pandoc $(addsuffix _prof.md,$(basename $@)) -o $(addsuffix _prof.html,$(basename $@)) --template $(TEMPLATEHTML) \
		--css $(TEMPLATECSS) --self-contained -N --toc --toc-depth 2
	pandoc $(addsuffix _etu.md,$(basename $@)) -o $(addsuffix _etu.html,$(basename $@)) --template $(TEMPLATEHTML) \
		--css $(TEMPLATECSS) --self-contained -N --toc --toc-depth 2

#%.html: %.md $(TEMPLATEHTML) $(TEMPLATECSS) 
#	pandoc	$< -o $@ --template $(TEMPLATEHTML) \
		--css $(TEMPLATECSS) --self-contained -N --toc --toc-depth 2 --metadata title="$@"

pdf: $(TRGETSPDF)  ## convert etapes-install/*_{prof, etu}.md to *.pdf

%.pdf: %.md $(TEMPLATEPDF)
	pandoc --pdf-engine=xelatex --template $(TEMPLATEPDF) -F pandoc-plantuml $(addsuffix _prof.md,$(basename $@)) -o $(addsuffix _prof.pdf,$(basename $@)) -N --toc-depth 2
	pandoc --pdf-engine=xelatex --template $(TEMPLATEPDF) -F pandoc-plantuml $(addsuffix _etu.md,$(basename $@)) -o $(addsuffix _etu.pdf,$(basename $@)) -N --toc-depth 2

help: ## --> this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

pull: ## git pull
	git pull

clean: ## rm *_etu.md and *_prof.md files
	rm -f $(NOTES)/*_prof.md $(NOTES)/*_etu.md $(TP)/*_prof.md $(TP)/*_etu.md

superclean: ## rm *_etu* and *_prof* files
	rm -f $(NOTES)/*prof* $(NOTES)/*etu* $(TP)/*prof* $(TP)/*etu*
#	rm -f $(TRGETSHTML) $(TRGETSPDF) $(SRC)/*prof.md $(SRC)/*etu.md
