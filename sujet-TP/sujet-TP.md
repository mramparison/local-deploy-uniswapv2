---
title: TP UniswapV2
date: Jan. 2023
author:  
- MR
---


[[_TOC_]]


<!--PROF

Remarques :

-->

# Objectifs

- Se familiariser avec un Decentralized Exchange (DEX) comme UniswapV2
- Comprendre les mécanismes
    - de création de tokens
    - de création de paires de tokens
    - addLiquidity, removeLiquidity
    - d'échange de tokens (swap)
- Intéragir avec un DEX en maîtrisant les scripts javascript/typescript
- Découvrir et comprendre les modèles derrière swap et add/removeLidquidity
- Simuler des transactions pour tester le DEX

# Mise en place 

Un marchine virtuelle vous est fournie tournant sous
Linux Mint à lancer depuis Virtualbox en utilisant le script **``/matieres/WMMFMB22/lance-vm-mint-uniswap.sh``** (*username: uniswapv2 & password: uniswap*, clavier US à la connexion) (ou téléchargeant l'image .ova, voir ci-dessous).

Selectionnez l'**option 2** (*[2] use a persistent machine folder [...]*) pour ne pas perdre le contenu de votre VM entre les séances.

Pour votre machine perso [vous pouvez télécharger la VM .ova ici](https://filesender.renater.fr/?s=download&token=54ac59a4-d82e-41d7-b7d6-762f40a73c7c) ou [ici](https://sdrive.cnrs.fr/s/2rQ4RMPXHEMt8yY).

- UniswapV2 est pré-déployé depuis l'environnement de développement d'Ethereum [Hardhat](https://hardhat.org/docs)
- On se rend dans le dossier 
```bash
cd uniswapv2-0.8
```
- Pour lancer un *node* local utiliser
```bash
npx hardhat node
```
qui lancera une interface JSON-RPC sur le port 8545 de localhost (127.0.0.1); la configuration est visible dans le fichier ``hardhat-config.ts``
- Pour lancer un script du dossier *scripts* qui s'exécutera sur le node utiliser
```bash
npx hardhat run scripts/nom_du_script --network localhost
```
- Vous pouvez également lancer une console directement
```bash
npx hardhat console
```

# Architecture du code d'UniswapV2

## Architecture générale

Principalement il y a :

- un dossier ``contracts`` contenant les différents *smart contracts (contrats intelligents)* utilisant le langage [Solidity](https://docs.soliditylang.org/en/v0.8.18/) (extension ``.sol``), proche de Javascript. C'est un langage orienté objet, à typage statique.

- un dossier ``artifacts`` contenant les smart contracts compilés. Plus précisément il contient les fichiers ``.json`` détaillant chaque smart contract compilé et contenant notamment le *bytecode* du smart contract qui sera exécuté sur la machine virtuelle Ethereum ([EVM](https://ethereum.org/fr/developers/docs/evm/))

- un dossier ``contracts/test`` contenant les différents tests que Hardhat peut lancer automatiquement

- un dossier ``scripts`` dans lequel on placera les scripts pour déployer les smart contracts sur le node local


## Architecture des dépendances entre les smart contracts

Les smart contracts utilisés possèdent différents niveau d'abstraction et de dépendances :

- ``UniswapV2ERC20.sol`` est le contrat définissant un token ERC20 (le standard utilisé pour les smart contracts sur le réseau Ethereum). Sont définies par exemple, les fonctions permettant de :

    - ``transfer`` le token d'une adresse à une autre; c'est à dire enlever un montant possédé par celui qui envoie et l'ajouter à celui qui reçoit
    - ``mint`` un token, c'est à dire "frapper la pièce de monnaie", qui représente la création officielle du token et va ajouter le montant à l'adresse qui mint et augmenter le *totalSupply (réserve totale)* disponible du jeton
    - ``burn`` un token, c'est à dire sa destruction qui va retirer le montant à l'adresse qui burn et diminuer le *totalSupply* disponible du jeton

- ``UniswapV2Pair.sol`` est le contrat définissant une paire de tokens, c'est à dire le *Liquidity Pool (réserve de liquidités)*. Il dépend bien sûr de ``UniswapV2ERC20.sol`` puisqu'une paire de token sera également un token en elle-même, qui sera créé : *le liquidity share (part de liquidité)*. Sont définies par exemple, les fonctions permettant de :

    - ``mint`` ce qui va créer des liquidity shares pour l'adresse qui a transféré des tokens à la paire. Elle dépend de la fonction *mint* de ``UniswapV2ERC20.sol``
    - ``burn`` ce qui va détruire des liquidity shares d'une adresse et va lui transférer des deux tokens de la paire. Elle dépend des fonctions *burn* et *transfer* de ``UniswapV2ERC20.sol``
    - ``swap`` échanger un token de la paire contre l'autre, c'est à dire qu'une adresse va envoyer d'un token à la paire, qui va lui renvoyer de l'autre. Elle dépend de la fonction *transfer* de ``UniswapV2ERC20.sol``

- ``UniswapV2Factory.sol`` est le contrat permettant notamment de créer des paires de tokens avec la fonction ``createPair`` et de définir les *fees (frais)* associés.

- ``UniswapV2Router.sol`` est le contrat le plus haut niveau, il dépend de tous les autres. Sont définies par exemple, les fonctions permettant de :

    - ``addLiquidity`` ce qui va ajouter des liquidités à une paire, en utilisant :
        - *transfer* de chaque token vers la paire
        - *mint* de la paire
    - ``removeLiquidity`` ce qui va retirer des liquidités d'une paire, en utilisant :
        - *burn* de la paire
        - *transfer* de chaque token concerné vers l'adresse
    - ``swap`` ce qui va échanger un token contre un autre en utilisant *swap* de la paire après diverses vérifications, mais également un token contre de l'ETH qui n'est pas un token ERC20.


# Création d'un token ERC20


Pour la création d'un smart contract de token sur la blockchain Ethereum, il est fortement conseillé de suivre son standard ERC20.
Les règles imposées par ce standard sont l'implémentation au minimum de :

- ``totalSupply`` : le montant total de jetons crées. Est initialisé à la création, et mis à jour avec les fonctions ``mint`` et ``burn``

- ``balanceOf`` : une méthode qui retourne le nombre de tokens d'une adresse

-  ``transfer`` : une méthodes qui transfère à une adresse un certain montant du ``totalSupply``

- ``transferFrom`` : une méthode qui permet à une adresse de transférer un certain montant de ses tokens à une autre adresse sous réserve d'``approval`` et du montant d'``allowance`` ci-dessous

- ``approve`` : une méthode qui autorise un smart contract à transférer un certain montant de tokens d'une adresse

- ``allowance`` : cette méthode vérifie le montant autorisé pour la méthode ``approval`` ci-dessus.

## Mise en place du contrat

Nous allons créer un premier contrat de token ERC20 : créez un nouveau fichier *solidity*, par exemple *FunToken.sol* dans le dossier *contracts/*. 

N'hésitez pas à personnaliser son nom (*FunToken*) et son symbole (*FUN*) !


```js
pragma solidity ^0.8.0;

import { ERC20 } from "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract FunToken is ERC20 {

    // Define the supply of FunToken: 1,000,000 
    uint256 constant initialSupply = 1000000 * (10**18);

    // Constructor will be called on contract creation
    constructor() ERC20("FunToken", "FUN") {
        _mint(msg.sender, initialSupply);
    }
}
```

## Deploiement du contrat

Tout d'abord lancer le node hardhat.

Dans le dossier *scripts/* on va ajouter le deploiement du token, dans le fichier ``deploy-uniswap.js``.
On récupère une adresse, ici la première disponible :

```js
   [account] = await ethers.getSigners();
   deployerAddress = account.address;
   console.log(`Deploying contracts using ${deployerAddress}`);
```
> un ``Signer`` est une classe qui a accès à une clé privée et qui peut signer des transactions, notamment pour autoriser un smart contract à utliser des tokens d'une adresse. 	 

Puis on déploie notre smart contract à partir de cette adresse
```js
  // Deploy FunToken
  console.log('Deploying FunToken...');
  const FunToken = await ethers.getContractFactory('FunToken');
  const funTokenInstance = await FunToken.deploy();
  await funTokenInstance.deployed();
  
  console.log(`FunToken deployed to: ${funTokenInstance.address}`)
```

> la fonction ``getContractFactory`` crée une instance du contrat déployé en paramètre et le lie au *Signer* par défaut. Nous verrons par la suite que l'on peut aussi se lier à d'autres Signers.

On peut maintenant, dans un autre terminal que celui où le node tourne, lancer notre script
```bash
npx hardhat run scripts/deploy-uniswap.js --network localhost
```
qui nous affichera l'adresse du premier Signer et l'adresse du token.

### Metamask
Ouvrez firefox et l'extension MetaMask. En se connectant au node local hardhat et en ajoutant l'adresse du Signer par défaut, on peut désormais importer le token grace à son adresse. 

## Utilisation du token

Dans le dossier *scripts/* on va ajouter un script pour tester le token, dans le fichier ``test-Funtoken.ts`` 

### Affichage des détails du token

- on récupère une instance du token déployé 

> La fonction ``getContractAt`` est équivalente (modulo syntaxe et paramètres) à ``getContractFactory`` mais en read-only c'est à dire que les transactions ne consommeront pas d'ETH (voir la [doc](https://hardhat.org/hardhat-runner/plugins/nomiclabs-hardhat-ethers#helpers))

- on affiche l'``owner`` (propriétaire) du smart contract

- on affiche les données du token comme son nom, son symbole et son totalSupply dans différents formats

- on affiche la ``balanceOf`` de owner, le montant de tokens possédés.

```js
    console.log('===> Getting the fun token contract...\n');
    const contractAddress = '<a remplacer>'; // Token address
    const funToken = await ethers.getContractAt('FunToken', contractAddress);
    console.log(`Token Address: ${funToken.address}\n`);
    console.log('===> Getting Token owner and Balance...\n');
    const signers = await ethers.getSigners();
    console.log(`Owner is : ${signers[0].address}\n`);
    const ownerAddress = signers[0].address;
    let ownerBalance = await funToken.balanceOf(ownerAddress);

    const name = await funToken.name();
    console.log(`Token Name: ${name}`);
    const symbol = await funToken.symbol();
    console.log(`Token Symbol: ${symbol}`);
    const decimals = await funToken.decimals();
    console.log(`Token Decimals: ${decimals}\n`);

    const totalSupply = await funToken.totalSupply();
    console.log(`Total Supply including all decimals: ${totalSupply}`);
    console.log(`Total supply including all decimals comma separated: ${ethers.utils.commify(totalSupply)}`);
    console.log(`Total Supply : ${ethers.utils.formatUnits(totalSupply, decimals)}\n`);

    console.log(`Contract owner at ${ownerAddress} has a ${symbol} balance of ${ethers.utils.formatUnits(ownerBalance)}\n`);
```

On peut maintenant lancer notre script
```bash
npx hardhat run scripts/test-FunToken.js --network localhost
```

### Un premier transfert

On va ici faire un transfert d'``owner`` à ``recipient`` (le *bénéficiaire*), c'est à dire de la première adresse fournie par le node hardhat à la seconde.

- on récupère l'adresse du bénéficiaire qui est le deuxième *Signer*, donc ``signer[1]``

- on initialise un transfert avec la fonction ``transfer(to, amount)``

- on affiche les ``balanceOf`` associées à *ownerAddress* et  *recipientAddress*


```js
    console.log('==> Initiating a transfer from 0 to 1...');
    const recipientAddress = signers[1].address;
    const transferAmount = 1000;
    console.log(`Transferring ${transferAmount} ${symbol} tokens to ${recipientAddress} from ${ownerAddress}`);
    await funToken.transfer(recipientAddress, ethers.utils.parseUnits(transferAmount.toString(), decimals));
    console.log('Transfer completed');
    ownerBalance = await funToken.balanceOf(ownerAddress);
    console.log(`Balance of owner (${ownerAddress}): ${ethers.utils.formatUnits(ownerBalance, decimals)} ${symbol}`);
    let recipientBalance = await funToken.balanceOf(recipientAddress);
    console.log(`Balance of recipient (${recipientAddress}): ${ethers.utils.formatUnits(recipientBalance, decimals)} ${symbol}\n`);    
```

**Remarques** 

Ici le transfert est possible car :

- *owner* a deployé le token

- *owner* possède un montant non nul de token

- *owner* initialise un transfert


On va ensuite faire un transfert depuis le bénéficiaire qui ne remplit pas les conditions ci-dessus, et va dnc devoir utiliser les méthodes *approve* et *allowance* décrites précédemment.

### Un second transfert

Vous allez maintenant réaliser un transfert de *recipient* (signers[1]) vers *owner* (signers[0]). Il faudra :

- vérifier initialement que owner ne peut bien utiliser les tokens de recipient avec la fonction  ``allowance(address owner, address spender)`` qui doit renvoyer 0

- autoriser owner et son smart contract à utiliser des tokens de recipient, avec la fonction ``approve(address spender, uint256 amount)``. Ici il faudra tout d'abord créer une nouvelle instance du token pour recipient en utilisant ``funToken.connect(signers[1])`` puis utiliser *approve* sur cette instance

```js
    console.log('==> Setting Allowance...');
    console.log(`Setting allowance amount of spender over the caller\'s ${symbol} tokens...`);
    const apprvAmount = 10000;
    const approveAmount = ethers.utils.parseUnits(apprvAmount.toString(), decimals)
    console.log(`This example allows the contractOwner to spend up to ${approveAmount} of the recipient\'s ${symbol} token`);
    // Creates a new instance of the contract connected to the recipient
    const signerContract =  // < à compléter >
    // Approves owner to spend approveAmount tokens over recipient's address
    await // < à compléter pour approveAmount >
    console.log(`Spending approved\n`);
```

- vérifier que owner peut bien utiliser les tokens de recipient avec la fonction  ``allowance(address owner, address spender)`` qui doit renvoyer le montant défini précedemment par *approve*

```js
    console.log(`Getting the contracOwner spending allowance over recipient\'s ${symbol} tokens...`);
    let allowance = await // à compléter
    console.log(`contractOwner Allowance: ${ethers.utils.formatUnits(allowance, decimals)} ${symbol}\n`);
```

- transférer un montant de recipient à owner avec la fonction ``transferFrom(address from, address to, uint256 amount)``

```js
    console.log('==> Initiating a transfer from 1 to 0...');
    const trsfrFromAmount = 100;
    const transferFromAmount = ethers.utils.parseUnits(trsfrFromAmount.toString(), decimals)
    console.log(`contracOwner transfers ${transferFromAmount} ${symbol} from recipient\'s account into own account...`);
    await // < à compléter pour transférer transferFromAmount >
    ownerBalance = await funToken.balanceOf(ownerAddress);
    console.log(`New owner balance (${ownerAddress}): ${ethers.utils.formatUnits(ownerBalance, decimals)} ${symbol}`);
    recipientBalance = await funToken.balanceOf(recipientAddress);
    console.log(`New recipient balance (${recipientAddress}): ${ethers.utils.formatUnits(recipientBalance, decimals)} ${symbol}`);
    allowance = await funToken.allowance(recipientAddress, ownerAddress);
    console.log(`Remaining allowance: ${ethers.utils.formatUnits(allowance, decimals)} ${symbol}\n`);
```

**Remarques** 

Ici nous avons mis en pratique un concept de base extrêmement important :


- *owner* a deployé le token

- *owner* initialise un transfert depuis son compte car le smart contract est à lui

- *recipient* possède des tokens

- *recipient* peut transférer ses tokens uniquement en autorisant le smart contract de *owner* à le faire pour lui


Nous allons voir dans la suite que tout est similaire : pour la création de paires qui appartiennent à *owner* et y ajouter des tokens, *recipient* va devoir autoriser le smart contract de la paire (et donc owner) à agir en son nom.

### Pour aller plus loin

*recipient* peut par la suite modifier le montant de l'*allowance* au fur et à mesure des transactions avec ``increase/decreaseAllowance(address spender, uint256 addedValue)``. En effet si l'allowance tombe à zéro (c'est à dire qu'owner a dépensé tous les tokens que recipient lui autorise à utiliser, ou alors cherche à en transférer plus) les transferts ne seront plus possibles


# Création d'une paire de tokens

Dans cette partie nous allons créer une paire de tokens, un *Liquidity Pool* dans lequel des utilisateurs pourront déposer des tokens de la paire et recevoir des *Liquidity Shares*, mais également échanger un token pour l'autre. 

**Remarque** 

Une paire de tokens sera un smart contract respectant également le standard ERC20. Contrairement au token de base pour lequel les méthodes ``burn``, ``mint`` et le paramètre ``totalSupply`` ne sont plus utilisés une fois déployé, la paire de tokens utilisera au cours de sa vie ces éléments afin de maintenir un totalSupply de Liquidity Shares en adéquation avec le montant de tokens dont elle dispose.

**Mise en place** 

Maintenant que l'on peut déployer et utiliser des tokens, nous allons mettre en place une paire. 

Dans le dossier *scripts/* on va ajouter le deploiement de la ``Factory``, dans le fichier ``deploy-uniswap.js``. C'est le smart contract central d'Uniswap, celui qu va permettre la création et la manipulation de paires de tokens. On la déploie avec le code suivant :

```js
   //Deploy Factory
   const factory = await ethers.getContractFactory('UniswapV2Factory');
   const factoryInstance = await factory.deploy(deployerAddress);
   await factoryInstance.deployed();
   console.log(`Factory deployed to : ${factoryInstance.address}`);
```

On déploie à nouveau notre code : on peut **couper le node qui tourne et le relancer** puis dans l'autre terminal :
```bash
npx hardhat run script/deploy-uniswap.js --network localhost
```

Dans le dossier *scripts/* on va ajouter le code ci-dessous dans le fichier ``test-Pair.ts``. Ce code va :

- récupérer le compte ``owner``
- récupérer une instance de la factory, à modifier avec la bonne adresse renvoyée par le script précédent
- déployer deux tokens ERC20 à partir du contrat ``contracts/test/ERC20.sol`` (on n'utilise plus le token ERC20 précédent mais celui d'Uniswap)
- créer une paire avec les deux tokens déployés
- retrouver quel token créé correspond à quel token de la paire à partir de leur adresse, puisque la création de la paire les mélange
- afficher un récapitulatif des balances

```js
const [wallet] = await ethers.getSigners();
    console.log('===> Getting the Factory contract...\n');
    const factoryAddress = '<à modifier>'; // Factory address 
    const factory = await ethers.getContractAt('UniswapV2Factory', factoryAddress);
    console.log(`Factory Address: ${factory.address}\n`);

    console.log('===> Deploying two tokens...\n');

    // si pas encore créé
    const tokenA = await (
        await ethers.getContractFactory("contracts/test/ERC20.sol:ERC20")
      ).deploy(expandTo18Decimals(10000)) as ERC20;
    // si deja cree
    //const tokenA = await ethers.getContractAt('contracts/test/ERC20.sol:ERC20','<adresse du token>')

    const addressA = await tokenA.address;
    console.log(`Token A Address: ${addressA}`);
    console.log('===> Deploying token A...\n');
    var name = await tokenA.name();
    console.log(`Token Name: ${name}`);
    var symbol = await tokenA.symbol();
    console.log(`Token Symbol: ${symbol}`);
    var decimals = await tokenA.decimals();
    console.log(`Token Decimals: ${decimals}\n`);
    
    console.log('===> Deploying token B...\n');
    // si pas encore créé
    const tokenB = (await (
          await ethers.getContractFactory("contracts/test/ERC20.sol:ERC20")
        ).deploy(expandTo18Decimals(10000))) as ERC20;
    // si deja cree
    //const tokenB =  await ethers.getContractAt('contracts/test/ERC20.sol:ERC20','<adresse du token>')

    const addressB = await tokenB.address;
    console.log(`Token B Address: ${addressB}`);  
    name = await tokenB.name();
    console.log(`Token Name: ${name}`);
    symbol = await tokenB.symbol();
    console.log(`Token Symbol: ${symbol}`);
    decimals = await tokenB.decimals();
    console.log(`Token Decimals: ${decimals}\n`);
    
    console.log('===> Create Pair token A / token B...\n');
    // si pas encore créé
    await factory.createPair(tokenA.address, tokenB.address);
    const pair = (await ethers.getContractFactory("UniswapV2Pair")).attach(
       await factory.getPair(tokenA.address, tokenB.address)
    );
    // si deja cree
    //const pairContract = await factory.getPair(tokenA.address, tokenB.address);
    //const pair = (await ethers.getContractFactory("UniswapV2Pair")).attach(pairContract);

    console.log(`Pair Address: ${pair.address}\n`);

    console.log('===> Determine who\'s who...\n');
    const token0Address = await pair.token0();
    const token0 = tokenA.address === token0Address ? tokenA : tokenB;
    tokenA.address === token0Address ? console.log("TokenA is") : console.log("TokenB is") ;
    console.log(`Token0 Address: ${token0.address}`);
    const token1 = tokenA.address === token0Address ? tokenB : tokenA;
    tokenA.address === token0Address ? console.log("TokenB is") : console.log("TokenA is") ;
    console.log(`Token1 Address: ${token1.address}\n`);
    
    // récapitulatif des balances
    let pairTSupply = await pair.totalSupply();
    console.log(`Pair Total Supply: ${pairTSupply}\n`);

    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)} ( ${shortenTo18Decimals(await pair.balanceOf(pair.address))} )`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()} ( ${shortenTo18Decimals(await pair.totalSupply())} )`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await pair.balanceOf(wallet.address))} )`);
    console.log(`Pair Token1 Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair Token0 Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner Token1 Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner Token0 Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )`);
```

On lance notre nouveau script
```bash
npx hardhat run script/test-Pair.ts --network localhost
```

Et notre paire est déployée. 

&#x1F6A8; Désormais il faut commenter le code entre les balises
```js
// si pas encore créé
// si deja cree
```
et décommenter celui qui suit après avoir modifié l'adresse du token, afin d'utiliser notre paire lorsque l'on relancera le script. En effet sinon le script créera deux nouveaux tokens et une paire à chaque exécution du script.


# Manipulation d'une paire de tokens

Nous allons analyser les comportements suivants:

- addLiquidity
- removeLiquidity
- swap

## addLiquidity 

On décompose addLiquidity en 3 étapes :

- transférer un montant du premier token à la paire
- transférer un montant du second token à la paire
- mint la paire pour ajouter créer des Liquidity Shares pour l'utilisateur


On utilisera la fonction ``mint`` que l'on peut trouver dans ``contracts/UniswapV2Pair.sol``. Analyser cette fonction : comment est calculé le montant de Liquidity Shares (le montant de tokens de la paire que l'on va attribuer au Liquidity Provider) :

- lorsque la paire vient d'être créée
- lorsque l'on ajoute des liquidités à une paire déjà existante

Cette fonction appelle la méthode de base ``_mint`` du fichier ``contracts/UniswapV2ERC20.sol``.
Que fait elle?

Enfin, afin de réaliser addLiquidity, compléter le code ci-dessous pour réaliser ces étapes que vous ajouterez à ``script/test-Pair.ts``


```js
    console.log('===> Add Liquidity TokenA and TokenB...\n');

    console.log('=> Transfer...\n');

    const amountA = expandTo18Decimals(2);
    console.log(`Add: ${amountA} TokenA`);
    await // < à compléter >
    const amountB = expandTo18Decimals(5);
    console.log(`Add: ${amountB} TokenB\n`);
    await // < à compléter >

    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)} ( ${shortenTo18Decimals(await pair.balanceOf(pair.address))} )`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()} ( ${shortenTo18Decimals(await pair.totalSupply())} )`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await pair.balanceOf(wallet.address))} )`);
    console.log(`Pair Token1 Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair Token0 Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner Token1 Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner Token0 Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )`);

    console.log('=> Mint...\n');
    await // < à compléter >

    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)} ( ${shortenTo18Decimals(await pair.balanceOf(pair.address))} )`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()} ( ${shortenTo18Decimals(await pair.totalSupply())} )`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await pair.balanceOf(wallet.address))} )`);
    console.log(`Pair Token1 Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair Token0 Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner Token1 Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner Token0 Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )`);
```


## removeLiquidity 

On décompose removeLiquidity en 3 étapes :

- transférer un montant de Liquidity Shares (token de la paire) à la paire
- burn le montant de Liquidity Shares
- renvoyer le montant de chaque token de la paire au pro rata du montant de Liquidity Shares reçu



On utilisera la fonction ``burn`` que l'on peut trouver dans ``contracts/UniswapV2Pair.sol``. Analyser cette fonction : 

- comment est calculé le montant de chaque token que l'on va renvoyer au Liquidity Provider?
- quelles étapes prend-elle en charge par rapport à ``mint`` dans la partie précédente?

Cette fonction appelle la méthode de base ``_burn`` du fichier ``contracts/UniswapV2ERC20.sol``.
Que fait elle?

Enfin, afin de réaliser removeLiquidity, ajoutez le code ci-dessous à ``script/test-Pair.ts``


```js
    console.log('===> Remove Liquidity TokenA and TokenB...\n');

    const expectedLiquidity = expandTo18Decimals(2);
    console.log(`Remove: ${expectedLiquidity}\n`);

    console.log('=> Transfer...\n');

    await pair.transfer(pair.address, expectedLiquidity.sub(MINIMUM_LIQUIDITY));

    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)} ( ${shortenTo18Decimals(await pair.balanceOf(pair.address))} )`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()} ( ${shortenTo18Decimals(await pair.totalSupply())} )`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await pair.balanceOf(wallet.address))} )`);
    console.log(`Pair Token1 Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair Token0 Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner Token1 Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner Token0 Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )`);


    console.log('=> Burn...\n');

    await pair.burn(wallet.address);

    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)} ( ${shortenTo18Decimals(await pair.balanceOf(pair.address))} )`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()} ( ${shortenTo18Decimals(await pair.totalSupply())} )`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await pair.balanceOf(wallet.address))} )`);
    console.log(`Pair Token1 Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair Token0 Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner Token1 Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner Token0 Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )`);

```

**Remarque**

Entre le transfert et le burn, le montant de jetons de la paire envoyé par le liquidity provider est stocké dans la paire, avant le burn et sa disparition du totalSupply de la paire.


## Swap 

Dans le dossier *scripts/* on va ajouter le code ci-dessous dans le fichier ``test-Swap.ts``. Ce code va :

- récupérer le compte ``owner``
- récupérer une instance de la factory, à modifier avec la bonne adresse
- récupérer les tokens et la paire créée dans la partie précédente, à modifier avec la bonne adresse
- retrouver quel token créé correspond à quel token de la paire à partir de leur adresse
- afficher un récapitulatif des balances
- Swap un montant de token 1 pour du token 0, c'est à dire :
    - transfert d'un montant de token 1 à la paire
    - appel à la fonction ``function swap (uint256 amount0Out, uint256 amount1Out, address to, bytes data)`` de la paire (``contracts/UniswapV2Pair.sol``) qui va juste s'occuper de transferer le montant de token 0 passé en paramètre et mettre à jour les réserves de la paire. 
- Swap un montant de token 0 pour du token 1, avec les mêmes étapes que précédemment.


Ici le swap réalise une opération fixe : on transfère un montant fixe ``swapAmountA = 3`` du token 1, avec 

```js 
await token1.transfer(pair.address, swapAmountA);
```

et on récupère un montant fixe ``expectedAmountB = 1`` du token 0, avec

```js
await pair.swap(expectedAmountB, 0, wallet.address, "0x");
```

Nous allons mettre en place le calcul du montant de token 0 en fonction du montant de token 1 envoyés avec la fonction de conservation à produit constant : ``K = qA * qB``

Vous avez à disposition :

- les reserves des tokens A et B dans la pool
```js
let reserves = await pair.getReserves(); 
// on y accède avec 
// reserves[0] pour token 0 et 
// reserves[1] pour token 1
```

ou alors
```js
await token0.balanceOf(pair.address)
await token1.balanceOf(pair.address)
```

- le totalSupply de la paire
```js
await pair.totalSupply()
```

- le montant de token 1 envoyés ``swapAmountA``

Calculez ``expectedAmountB`` à donner en paramètre à la fonction swap, et réalisez le swap.

Pour respecter au mieux notre modèle, on pourra modifier le calcul initial du totalSupply dans la fonction ``mint`` de la paire dans le fichier ``contracts/UniswapV2Pair.sol``.


```js
const [wallet] = await ethers.getSigners();
    console.log('===> Getting the Factory contract...\n');
    const factoryAddress = '<à modifier>'; // Factory address
    const factory = await ethers.getContractAt('UniswapV2Factory', factoryAddress);

    console.log(`Factory Address: ${factory.address}\n`);

    const tokenA = await ethers.getContractAt('contracts/test/ERC20.sol:ERC20','<à modifier>')
    const addressA = await tokenA.address;
    console.log(`Token A Address: ${addressA}`);
    
    const tokenB =  await ethers.getContractAt('contracts/test/ERC20.sol:ERC20','<à modifier>')
    const addressB = await tokenB.address;
    console.log(`Token B Address: ${addressB}`);  
    
    const pairContract = await factory.getPair(tokenA.address, tokenB.address);
    const pair = (await ethers.getContractFactory("UniswapV2Pair")).attach(pairContract);

    console.log(`Pair Address: ${pair.address}`);
    
    const token0Address = await pair.token0();
    const token0 = tokenA.address === token0Address ? tokenA : tokenB;
    tokenA.address === token0Address ? console.log("TokenA is") : console.log("TokenB is") ;
    console.log(`Token0 Address: ${token0.address}`);
    const token1 = tokenA.address === token0Address ? tokenB : tokenA;
    tokenA.address === token0Address ? console.log("TokenB is") : console.log("TokenA is") ;
    console.log(`Token1 Address: ${token1.address}\n`);
    
    let pairTSupply = await pair.totalSupply();
    console.log(`Pair Total Supply: ${pairTSupply}\n`);


    let reserves = await pair.getReserves(); 
    console.log(`Reserve 1 : ${reserves[1]} ( ${shortenTo18Decimals(reserves[1])} )`);
    console.log(`Reserve 0 : ${reserves[0]} ( ${shortenTo18Decimals(reserves[0])} )`);


    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)} ( ${shortenTo18Decimals(await pair.balanceOf(pair.address))} )`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()} ( ${shortenTo18Decimals(await pair.totalSupply())} )`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await pair.balanceOf(wallet.address))} )`);
    console.log(`Pair Token1 Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair Token0 Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner Token1 Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner Token0 Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )\n`);


    console.log('===> Swap token 1 for token 0...\n');

    console.log('=> Transfer...\n');

    const swapAmountA = expandTo18Decimals(3);
    await token1.transfer(pair.address, swapAmountA);
    console.log(`Transfer ${swapAmountA} Token 1 \n`);

    console.log(`Pair Token1 Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair Token0 Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner Token1 Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner Token0 Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )\n`);

    console.log('=> Swap...\n');

    // <à modifier>
    const expectedAmountB = expandTo18Decimals(1);
    // <à modifier>
    // C'est expectedAmoutB qu'il faut calculer ci-dessus
    // et que l'on donne à la fonction swap après

    await pair.swap(expectedAmountB, 0, wallet.address, "0x");
    console.log(`Swap for ${expectedAmountB} Token 0 \n`);

    
    console.log(`Pair Token1 Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair Token0 Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner Token1 Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner Token0 Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )\n`);

    console.log('===> Swap token 0 for token 1...\n');

    console.log('=> Transfer...\n');

    const swapAmountB = expandTo18Decimals(3);
    await token0.transfer(pair.address, swapAmountB);
    console.log(`Transfer ${swapAmountB} Token 0 \n`);

    console.log(`Pair Token1 Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair Token0 Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner Token1 Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner Token0 Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )\n`);

    console.log('=> Swap...\n');

    // <à modifier>
    const expectedAmountA = expandTo18Decimals(1);
    // pareil
    // <à modifier>

    await pair.swap(0, expectedAmountA, wallet.address, "0x");
    console.log(`Swap for ${expectedAmountA} Token 1 \n`);

    
    console.log(`Pair Token1 Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair Token0 Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner Token1 Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner Token0 Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )\n`);

    reserves = await pair.getReserves(); 
    console.log(`Reserve 1 : ${reserves[1]} ( ${shortenTo18Decimals(reserves[1])} )`);
    console.log(`Reserve 0 : ${reserves[0]} ( ${shortenTo18Decimals(reserves[0])} )`);
```

Le calcul que nous réalisons ici pour déterminer le taux de change est un peu différent de celui utilisé en pratique : en effet, il y aura des *fees* (frais) pour les utilisateurs, un montant minimum de tokens qui doit être maintenu dans la pool, l'utilisation explicite de l'[*oracle*](https://medium.com/@epheph/using-uniswap-v2-oracle-with-storage-proofs-3530e699e1d3) pour le taux de change, etc.


### Metamask
Ouvrez firefox et l'extension MetaMask. Vous pouvez importer vos tokens dans Metamask à partir de leurs adresses.

# Interactions haut niveau avec UniswapV2 

Maintenant que nous avons pu décomposer brique par brique les différents éléments qui composent UniswapV2, et que nous les avons manipulés, nous allons pouvoir utiliser l'interface de plus haut niveau fournie par UniswapV2 : le [**Router**](https://docs.uniswap.org/contracts/v2/reference/smart-contracts/router-02). 

Il s'agit du premier *back-end* de l'interface d'UniswapV2, il contient toutes les fonctions permettant de add/removeLiquidity et d'échanger des tokens (swap) en utilisant les fonctions les plus robustes d'Uniswap (vérifications, calcul de *fees*, etc.). 


## Mise en place du Router

Dans le dossier *scripts/* on va ajouter le deploiement du router, dans le fichier ``deploy-uniswap.js``, mais également des autres contrats dont il dépend (WETH).

Notre fichier ``deploy-uniswap.js`` devient

```js
   [account] = await ethers.getSigners();
   deployerAddress = account.address;
   console.log(`Deploying contracts using ${deployerAddress}`);

   //Deploy WETH
   const weth = await ethers.getContractFactory('WETH9');
   const wethInstance = await weth.deploy();
   await wethInstance.deployed();

   console.log(`WETH deployed to : ${wethInstance.address}`);


  // Deploy FunToken
  // optionnel
  console.log('Deploying FunToken...');
  const FunToken = await ethers.getContractFactory('FunToken');
  const funTokenInstance = await FunToken.deploy();
  await funTokenInstance.deployed();
  
  console.log(`FunToken deployed to: ${funTokenInstance.address}`)


   //Deploy Factory
   const factory = await ethers.getContractFactory('UniswapV2Factory');
   const factoryInstance = await factory.deploy(deployerAddress);
   await factoryInstance.deployed();

   console.log(`Factory deployed to : ${factoryInstance.address}`);

   //Deploy Router passing Factory Address and WETH Address
   const router = await ethers.getContractFactory('UniswapV2Router');
   const routerInstance = await router.deploy(
      factoryInstance.address,
      wethInstance.address,
   );
   await routerInstance.deployed();

   console.log(`Router deployed to :  ${routerInstance.address}`);

   //Deploy Multicall (needed for Interface)
   const multicall = await ethers.getContractFactory('Multicall');
   const multicallInstance = await multicall.deploy();
   await multicallInstance.deployed();

   console.log(`Multicall deployed to : ${multicallInstance.address}`);
   ```

On peut arrêter le node, le relancer puis déployer dessus pour avoir un environnement

## Manipulation du Router

Ouvrez et lancez le script ``scripts/test-Global-Router.ts``

Ce script va refaire exactement toutes les étapes que nous avons réalisées jusqu'ici : 

- deployer deux tokens
- créer une paire
- transférer des liquidités d'une adresse à une autre
- ajouter des liquidités à la paire
- échanger des tokens


Nous utilisons désormais trois comptes :

- ``owner`` déploie tous les contrats
- ``other1`` peut transférer à ``other2``
- ``other2`` peut ajouter des liquidités à la paire
- ``other1`` peut swap

Cela est possible car nous gérons correctement les ``approve`` et ``allowance`` présentés dans la partie transfert de tokens.

Dans le code suivant

```js
await pair.connect(other2).approve(router02.address, ethers.constants.MaxUint256);
```

``other2`` autorise ``router02`` à utiliser ses tokens de la paire (montant maximal), pour pouvoir les transférer avant un burn.

Repérez dans le code les différentes **autorisations nécessaires** à donner au router avant les opérations de ``transfer``, ``addLiquidity`` et ``swap``. Sans ces autorisations, les opérations avec le router ne sont plus possibles.


Comparez le résultat du swap de tokens fait par le router avec celui que vous avez programmé dans la ``test-Swap.ts``

Manipulez les swaps dans differentes directions pour regarder l'évolution du prix dans la pool et en dehors, notamment : 

- un ajout de liquidités
- puis un swap important
- suivi d'un retrait de liquidités

## Simulation de transactions dans un Liquidity Pool

Nous allons ici lancer une simulation à partir de transactions d'un pool USCD/WETH. Ces transactions se trouvent dans le fichier ``USDC_WETH_pool.csv``, à placer dans le répertoire *scripts*.
Nous utiliserons ici le script ``test-Global-simu.ts``. 

Ce fichier contient l'ensemble des transactions dans la pool pour les journées du 10 et 11/02/2023. Pour notre simulation nous n'avons que 20 utilisateurs possibles, aussi :

- owner sera en charge d'initialiser la pool et des actions de *burn*
- les utilisateurs seront choisis **au hasard parmi nos 20 adresses** pour réaliser les actions de *mint* et de *swap*

**Remarque** : owner fait tous les burns car sinon il faut aussi que les autres utilisateurs mint initialement afin de pouvoir burn ensuite (c'est à dire posséder des LP shares pour pouvoir en burn), et donc stocker un historique supplémentaire dans une variable.

Avant de lancer le script, il faut ajouter un module pour :

- parser le fichier
- dessiner un graphique
- exporter un fichier csv

```bash
yarn add csv-parse
npm install --save-dev chartjs-node-canvas
npm install --save-dev csv-stringify
```

### Description du script

Le script va :

- ouvrir le fichier et créer un objet ``Result`` avec le contenu du fichier
- initialiser l'état de la pool juste avant le début des transactions, c'est à dire le 10/02 à 00h00

```js
[{'symbol': 'USDC',
  'address': '_adresse_de_owner_',
  'amount': 43717959.978289},
 {'symbol': 'WETH',
  'address': '_adresse_de_owner_',
  'amount': 28316.506300133937}]
```

- parcourir et exécuter les transactions une à une à partir d'une adresse aléatoire parmi les 20 disponibles, sauf pour le burn.
- dessiner le graphique de l'évolution du taux de change dans le fichier ``scripts/out.png``
- exporter l'ensemble des taux de changes dans le fichier ``scripts/ExRate_WETHUSDC.csv``


**Remarque** : nous utilisons le même token ERC20 qu'auparavant et non le contrat WETH qui est disponible; en effet WETH est juste un contrat qui "enveloppe" de l'ETH au ratio 1:1 donc nécessite des manipulations supplémentaires dans le script.

### Exécution du script

Le fichier CSV à parser comporte plus de 6000 transactions. Effectuer les transactions peut prendre plusieurs dizaines de minutes.

Avant de lancer le script qui va réaliser l'ensemble des transactions, mieux vaut tester son bon fonctionnement sur quelques lignes.
En fin de fichier, vous trouverez la condition suivante :

```js
if (noMoreTxPls > 30) break; // bound the number of transactions
```

qui arrête le script au bout de quelques transactions. Pour effectuer l'ensemble des transactions, il faudra commenter la ligne.

Éxecutez le script pour ces quelques transactions, qui doit fonctionner sans erreur, et vérifiez que les fichiers ``scripts/out.png`` et ``scripts/ExRate_WETHUSDC.csv`` sont bien créés.

### Interprétation des résultats

- Le fichier ``scripts/out.png`` propose un graphique, potentiellement peu lisible
- Le fichier ``scripts/ExRate_WETHUSDC.csv`` peut être ouvert avec OpenOffice en choisissant ``Separated by -> Comma`` puis en réalisant un graphique avec le bouton *Chart*, normalement plus lisible

Comparez les résultats obtenus avec le vrai pool WETH/USDC, [par exemple ici](https://coinmarketcap.com/dexscan/ethereum/0x88e6a0c2ddd26feeb64f039a2c41296fcb3f5640).
